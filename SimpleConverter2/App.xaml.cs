﻿using SimpleConverter.ViewModel;
using SimpleConverter.View;
using System.Windows;

namespace SimpleConverter
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var window = new MainWindow();
            var viewModel = new ConverterViewModel();
            window.DataContext = viewModel;
            window.Show();
        }
    }
}
