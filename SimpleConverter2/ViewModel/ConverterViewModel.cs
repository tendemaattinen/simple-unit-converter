﻿using System.Collections.Generic;
using SimpleConverter.Utility;
using SimpleConverter.Model;
using System.Windows.Input;
using System;

namespace SimpleConverter.ViewModel
{
    public class ConverterViewModel : ViewModelBase
    {
        readonly IJsonReader _jsonreader;
        readonly IConverterModel _converter;
        public ConverterViewModel()
        {
            IFactory factory = new Factory();
            _jsonreader = factory.CreateNewJsonReader();
            _converter = factory.CreateNewConverterModel();
        }

        public string Answer { get; set; }
        public double Value { get; set; }
        public string SelectedFromUnit { get; set; }
        public string SelectedToUnit { get; set; }


        private string _selectedMeasurement;
        public string SelectedMeasurement
        {
            get
            {
                return _selectedMeasurement;
            }
            set
            {
                _selectedMeasurement = value;
                OnPropertyChanged("Units");
            }
        }

        private List<string> _measurements = null;
        public List<string> Measurements
        {
            get
            {
                if (_measurements == null)
                {
                    _measurements = _jsonreader.GetMeasurements();
                    OnPropertyChanged("Measurements");
                }
                return _measurements;
            }
            set
            {
                _measurements = value;
                OnPropertyChanged("Measurements");
            }
        }

        private List<string> _units;
        public List<string> Units
        {
            get
            {
                _units = _jsonreader.GetUnits(SelectedMeasurement);
                return _units;
            }
            set
            {
                _units = value;
                OnPropertyChanged("Units");
            }
        }

        private ICommand _convertValue;
        public ICommand ConvertValue
        {
            get
            {
                if (_convertValue == null)
                {
                    _convertValue = new RelayCommand(
                        param => CalculateAnswer(),
                        param => CanCalculateAnswer()
                        );
                }
                return _convertValue;
            }
        }

        private void CalculateAnswer()
        {
            Answer = Math.Round(_converter.CalculateAnswer(Value, 
                _jsonreader.GetFactor(SelectedMeasurement, SelectedFromUnit), 
                _jsonreader.GetFactor(SelectedMeasurement, SelectedToUnit)
                ), 8, MidpointRounding.AwayFromZero) + " " + SelectedToUnit;
            OnPropertyChanged("Answer");
        }

        private bool CanCalculateAnswer()
        {
            if (string.IsNullOrEmpty(SelectedMeasurement) && string.IsNullOrEmpty(SelectedFromUnit) && string.IsNullOrEmpty(SelectedMeasurement))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
