﻿using System.Collections.Generic;

namespace SimpleConverter.Utility
{
    public interface IJsonReader
    {
        double GetFactor(string measurement, string unit);
        List<string> GetMeasurements();
        List<string> GetUnits(string measurement);
    }
}