﻿namespace SimpleConverter.Utility
{
    public interface IFactory
    {
        Model.IConverterModel CreateNewConverterModel();
        IJsonReader CreateNewJsonReader();
    }
}