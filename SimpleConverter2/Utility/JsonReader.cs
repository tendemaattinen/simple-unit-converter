﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.IO;

namespace SimpleConverter.Utility
{
    class JsonReader : IJsonReader
    {
        private const string FILE_LOCATION = "JSON/ConvertFactors.json";
        private readonly JObject jObj;

        public JsonReader()
        {
            jObj = JObject.Parse(File.ReadAllText(FILE_LOCATION));
        }

        public List<string> GetMeasurements()
        {
            var list = new List<string>();
            foreach (KeyValuePair<string, JToken> tag in jObj)
            {
                list.Add(tag.Key);
            }
            return list;
        }

        public List<string> GetUnits(string measurement)
        {
            List<string> list = new List<string>();
            try
            {
                var results = jObj[measurement].Children();
                foreach (JToken result in results)
                {
                    list.Add(result.ToObject<JProperty>().Name);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return list;
        }

        public double GetFactor(string measurement, string unit)
        {
            var factor = 0.0;
            try
            {
                var results = jObj[measurement].Children();
                foreach (JToken result in results)
                {
                    if (result.ToObject<JProperty>().Name == unit)
                    {
                        double.TryParse(result.ToObject<JProperty>().Value.ToString(), out factor);
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return factor;
        }
    }
}
