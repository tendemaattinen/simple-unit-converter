﻿using SimpleConverter.Model;

namespace SimpleConverter.Utility
{
    public class Factory : IFactory
    {
        public IJsonReader CreateNewJsonReader()
        {
            return new JsonReader();
        }

        public IConverterModel CreateNewConverterModel()
        {
            return new ConverterModel();
        }
    }
}
