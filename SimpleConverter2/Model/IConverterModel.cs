﻿namespace SimpleConverter.Model
{
    public interface IConverterModel
    {
        double CalculateAnswer(double value, double factorFrom, double factorTo);
    }
}