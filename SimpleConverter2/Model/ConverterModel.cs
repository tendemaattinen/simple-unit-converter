﻿using System;

namespace SimpleConverter.Model
{
    public class ConverterModel : IConverterModel
    {
        public double CalculateAnswer(double value, double factorFrom, double factorTo)
        {
            var ans = value / factorFrom;
            ans *= factorTo;
            return ans;
        }
    }
}
