﻿using SimpleConverter.Model;
using SimpleConverter.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SimpleConverterTests
{
    public class ConverterModelUnitTests
    {
        [Theory]
        [InlineData (43, 0.2, 0.4, 86)]
        [InlineData(50, 2, 0.6, 15)]
        public void CalculateAnswer_SingleValue_RightAnswer(double value, double factor1, double factor2, double answer)
        {
            var fac = new Factory();
            var calc = fac.CreateNewConverterModel();
            Assert.Equal(answer, calc.CalculateAnswer(value, factor1, factor2));
        }
    }
}
